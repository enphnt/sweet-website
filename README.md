# sweet-website
| 🏗️ | [![pipeline status](https://gitlab.com/enphnt/sweet-website/badges/master/pipeline.svg)](https://gitlab.com/enphnt/sweet-website/commits/master) |
|---|---|
| ✅ | [![coverage report](https://gitlab.com/enphnt/sweet-website/badges/master/coverage.svg)](https://gitlab.com/enphnt/sweet-website/commits/master) |



The app is built, tested and deployed to gitlab pages. It is an example implementation of continuous integration (CI). The gitlab pipeline steps are: 
  - Build
  - Test 
  - Deploy

